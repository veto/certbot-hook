#!/bin/bash
dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo "start" > auth.log
source "${dir}/config.cfg"
domain="${CERTBOT_DOMAIN}"
val="${CERTBOT_VALIDATION}"
acme="_acme-challenge.${CERTBOT_DOMAIN}"
zone=$(expr match "${CERTBOT_DOMAIN}" '.*\.\(.*\..*\)')
echo "token: $token" >> auth.log
echo "domain: $domain" >> auth.log
echo "val: $val" >> auth.log
echo "acme: $acme" >> auth.log
echo "DIR: $dir" >> auth.log

GET=$(cat <<EOF 
{
    "authToken": "$token",
    "limit": 1,
    "page": 1,


    "filter": {
        "field": "RecordName",
        "value": "$acme"
    },

    "sort": {
        "field": "zoneName",
        "order": "asc"
    }
}
EOF
)

echo "GET: $GET" >> auth.log
OLD=$(curl -d "$GET" -s 'https://partner.routing.net/api/dns/v1/json/zonesFind' | tac | sed -nE '/"'$acme'"/, /addDate/p ' | sed -n -e 's/"content":\(.*\),/\1/p' )
#OLD=${OLD//\"}
OLD=${OLD// }
#OLD=${OLD//\\}
echo "OLD: $OLD" >> auth.log

if [ -z "$OLD" ]
then
DATA=$(cat <<EOF 
{
    "authToken": "$token",
    "zoneConfig": {
        "name": "$zone"
    },
    "recordsToAdd": [
        {
            "name": "$acme",
            "type": "TXT",
            "content": "$val",
            "ttl": 86000
        }
    ]
}
EOF
)
       
else
DATA=$(cat <<EOF 
{
    "authToken": "$token",
    "zoneConfig": {
        "name": "$zone"
    },
    "recordsToAdd": [
        {
            "name": "$acme",
            "type": "TXT",
            "content": "$val",
            "ttl": 86000
        }
    ],
    "recordsToDelete": [
        {
            "name": "$acme",
            "type": "TXT",
            "content": $OLD
        }
    ]
}
EOF
)
fi

echo "DATA: $DATA" >> auth.log

UPDATE=$(curl -d "$DATA" -s 'https://partner.routing.net/api/dns/v1/json/zoneUpdate')

echo "Update: $UPDATE" >> auth.log

echo "end" >> auth.log
sleep 30

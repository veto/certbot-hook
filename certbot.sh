#!/bin/bash 
if [ $# -eq 0 ]
then
    echo "missing domain parameter!"
    echo "  example: "
    echo "          ./certbot.sh foo.bar.com"

    echo $DOMAIN
else
certbot certonly \
	--manual \
	--manual-auth-hook ./auth.sh \
	--preferred-challenges=dns  \
        --keep-until-expiring \
	--register-unsafely-without-email \
	--agree-tos --manual-public-ip-logging-ok \
	-d $1
fi

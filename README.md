# certbot-hook
 certbot validation bash script hooks for updating a DNS server via http://docs.routing.net/api

## usage:
 1. install certbot. for debian run apt-get install certbot
 2. rename default-config.cfg to config.cfg
 3. set your API Key into config.cfg
 4. run ./certbot.sh foo.bar.com
 

 
